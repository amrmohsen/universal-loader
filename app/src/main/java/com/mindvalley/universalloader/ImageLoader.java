package com.mindvalley.universalloader;

import android.app.Activity;
import android.content.AsyncTaskLoader;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ImageLoader extends AsyncTaskLoader<Bitmap> {

        String urlStr;

        public ImageLoader(Activity context, String urlStr) {
            super(context);
            this.urlStr = urlStr;
            onContentChanged();
        }

        @Override
        protected void onStartLoading() {
            super.onStartLoading();
            if (takeContentChanged())
                forceLoad();
        }

        @Override
        public Bitmap loadInBackground() {

            Bitmap bitmap = null;
            try {
                URL url = new URL(urlStr);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                bitmap = BitmapFactory.decodeStream(input);
            } catch (Exception ex) {
                Log.d("error", ex.toString());
            }
            return bitmap;
        }
    }