package com.mindvalley.universalloader;

/**
 * Created by Amr on 8/27/16.
 */

public interface ErrorResponseListener {


    public void onError(Exception e);

}
