package com.mindvalley.universalloader;

/**
 * Created by Amr on 8/28/16.
 */

public class User {

    private String userID;
    private String name;
    private String profileURL;

    public User() {
    }

    public User(String userID, String name, String profileURL) {
        this.userID = userID;
        this.name = name;
        this.profileURL = profileURL;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileURL() {
        return profileURL;
    }

    public void setProfileURL(String profileURL) {
        this.profileURL = profileURL;
    }
}
