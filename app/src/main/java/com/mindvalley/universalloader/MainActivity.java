package com.mindvalley.universalloader;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mindvalley.com.universalloaderlib.listeners.ErrorResponseListener;
import mindvalley.com.universalloaderlib.listeners.ResponseListener;
import mindvalley.com.universalloaderlib.request.datatype.JsonRequest;

public class MainActivity extends AppCompatActivity {

    private String KEY_USER = "user";
    private String KEY_USER_ID = "id";
    private String KEY_USER_NAME = "name";
    private String KEY_PROFILE = "profile_image";
    private String KEY_PROFILE_DIM = "large";

    RecyclerView gridItems;
    private String API_URL = "http://pastebin.com/raw/wgkJgazE";
    private int REQUEST_ID = 500;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridItems = (RecyclerView) findViewById(R.id.grid_items);
        gridItems.addItemDecoration(new MarginDecoration(this));
        gridItems.setHasFixedSize(true);
        gridItems.setLayoutManager(new GridLayoutManager(this, 2));


        JsonRequest jsonRequest = new JsonRequest(REQUEST_ID, this, new ResponseListener() {
            @Override
            public void onSuccess(Object o) {
                GridAdapter gridAdapter = new GridAdapter(MainActivity.this, extractDataFromJson(o.toString()));
                gridItems.setAdapter(gridAdapter);
            }
        }, new ErrorResponseListener() {
            @Override
            public void onError(Exception e) {
                Toast.makeText(MainActivity.this, "Couldn't load data!", Toast.LENGTH_SHORT).show();
            }
        });
        jsonRequest.load(API_URL);


    }

    private ArrayList<User> extractDataFromJson(String jsonStr) {

        ArrayList<User> users = new ArrayList<>();

        try {
            JSONArray usersData = new JSONArray(jsonStr);

            for (int i = 0; i < usersData.length(); i++) {
                User user = new User();
                JSONObject object = usersData.getJSONObject(i);
                JSONObject userObject = object.getJSONObject(KEY_USER);
                user.setUserID(userObject.getString(KEY_USER_ID));
                user.setName(userObject.getString(KEY_USER_NAME));
                JSONObject objectProfile = userObject.getJSONObject(KEY_PROFILE);
                user.setProfileURL(objectProfile.getString(KEY_PROFILE_DIM));
                Log.d("username",user.getName());
                users.add(user);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return users;

    }


}
