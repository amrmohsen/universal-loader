package com.mindvalley.universalloader;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import mindvalley.com.universalloaderlib.listeners.ErrorResponseListener;
import mindvalley.com.universalloaderlib.listeners.ResponseListener;
import mindvalley.com.universalloaderlib.request.datatype.ImageRequest;

public class GridAdapter extends RecyclerView.Adapter<GridAdapter.UserHolder> {

    private Activity context;
    private List<User> users;

    public GridAdapter(Activity context, List<User> users) {
        this.context = context;
        this.users = users;
    }


    //Holder class
    public static class UserHolder extends RecyclerView.ViewHolder {

        final View mView;
        final TextView tvFullName;
        final ImageView ivProfilePic;
        final Context context;

        UserHolder(View itemView) {
            super(itemView);
            mView = itemView;
            context = itemView.getContext();
            tvFullName = (TextView) mView.findViewById(R.id.tv_name);
            ivProfilePic = (ImageView) mView.findViewById(R.id.iv_profile);

        }
    }

    @Override
    public UserHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.grid_item, parent, false);

        return new UserHolder(v);
    }

    @Override
    public void onBindViewHolder(final UserHolder holder, final int position) {


        holder.tvFullName.setText(users.get(position).getName());

        Log.d("pos",position+"");
        //use the universal loader here
        ImageRequest imageRequest = new ImageRequest(position, context, new ResponseListener() {
            @Override
            public void onSuccess(Object o) {
                holder.ivProfilePic.setImageBitmap((Bitmap) o);
            }
        }, new ErrorResponseListener() {
            @Override
            public void onError(Exception e) {

            }
        });
        imageRequest.load(users.get(position).getProfileURL());


    }

    @Override
    public int getItemCount() {
        return users.size();
    }
}