package com.mindvalley.universalloader;

import android.app.Activity;
import android.os.Bundle;

/**
 * Created by Amr on 8/26/16.
 */

public class JsonRequest extends Request {


    JsonLoader jsonLoader;

    public JsonRequest(int id,Activity context,ResponseListener responseListener, ErrorResponseListener errorResponseListener) {
        super(id,context,responseListener,errorResponseListener);
    }

    @Override
    public void load(final String url) {

        context.getLoaderManager().initLoader(1, null, new android.app.LoaderManager.LoaderCallbacks<String>() {

            @Override
            public android.content.Loader<String> onCreateLoader(int i, Bundle bundle) {
                jsonLoader = new JsonLoader(context, url);
                return jsonLoader;
            }

            @Override
            public void onLoadFinished(android.content.Loader<String> loader, String jsonStr) {

                try {
                    responseListener.onSuccess(jsonStr);
                } catch (Exception e) {
                    errorResponseListener.onError(e);
                }
            }

            @Override
            public void onLoaderReset(android.content.Loader<String> loader) {

            }
        });

    }

    public void cancelLoad() {

        jsonLoader.reset();
    }

}
