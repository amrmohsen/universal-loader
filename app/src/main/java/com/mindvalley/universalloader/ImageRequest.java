package com.mindvalley.universalloader;

import android.app.Activity;
import android.content.Loader;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by Amr on 8/26/16.
 */

public class ImageRequest extends Request {

    ImageLoader imageLoader;

    public ImageRequest(int id,Activity context,ResponseListener responseListener, ErrorResponseListener errorResponseListener) {
        super(id,context,responseListener,errorResponseListener);
    }

    @Override
    public void load(final String url) {

        Log.d("id",id+"");
        context.getLoaderManager().initLoader(id, null, new android.app.LoaderManager.LoaderCallbacks<Bitmap>() {

            @Override
            public android.content.Loader<Bitmap> onCreateLoader(int i, Bundle bundle) {
                imageLoader = new ImageLoader(context, url);
                return imageLoader;
            }

            @Override
            public void onLoadFinished(Loader<Bitmap> loader, Bitmap bitmap) {

                try {
                    if (!bitmap.equals(null))
                        responseListener.onSuccess(bitmap);
                } catch (Exception e) {
                    errorResponseListener.onError(e);
                }
            }

            @Override
            public void onLoaderReset(Loader<Bitmap> loader) {

            }
        });

    }

    public void cancelLoad() {

        imageLoader.reset();
    }

}
