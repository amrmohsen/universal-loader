package com.mindvalley.universalloader;

import android.app.Activity;

/**
 * Created by Amr on 8/26/16.
 */

public abstract class Request {

    public int id;
    public Activity context;

    public ResponseListener responseListener;
    public ErrorResponseListener errorResponseListener;

    public Request(Activity context) {
        this.context = context;
    }

    public Request(int id,Activity context, ResponseListener responseListener, ErrorResponseListener errorResponseListener) {
        this.id=id;
        this.context = context;
        this.responseListener = responseListener;
        this.errorResponseListener = errorResponseListener;
    }

    public void setResponseListener(ResponseListener responseListener) {
        this.responseListener = responseListener;
    }

    public void setErrorResponseListener(ErrorResponseListener errorResponseListener) {
        this.errorResponseListener = errorResponseListener;
    }

    public abstract void load(String itemUrl);

}
